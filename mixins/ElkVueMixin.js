import LoadingElementMixin from '@paiuolo/pai-vue-mixins/mixins/LoadingElementMixin'
import EntityListMixin from '@paiuolo/pai-vue-mixins/mixins/EntityListMixin'
import xhrPostMixin from '@paiuolo/pai-vue-mixins/mixins/xhrPostMixin'

export default {
  mixins: [
    LoadingElementMixin,
    EntityListMixin,
    xhrPostMixin
  ],
  props: {
    esUrl: {
      type: String,
      default: 'https://www.example.org/elasticsearch'
    },
    auto: {
      type: Boolean,
      default: false
    },
    sort: {
      type: Array,
      default: null
    },
    esQuery: {
      type: Object,
      default () {
        return {
          from : 0,
          size : 10,
          query: {
            match_all: {}
          }
        }
      }
    },
    size: {
      type: [String, Number],
      default: 10
    },
    from: {
      type: [String, Number],
      default: 0
    }
  },
  data () {
    return {
      xhrPostOptions: {
        withCredentials: true
      },
      xhrPostUrl: this.esUrl,
      xhrPostPermittedErrors: '502',
      xhrPostMaxRetries: 5,
      xhrPostRetryTimeout: 199,
      xhrPostRetryOnPermittedError: true,
      query: this.esQuery
    }
  },
  mounted () {
    this.$on('performingXhrPost', () => {
      this.startLoading()
      this.$emit('performingGetEntities')
    })

    this.$on('xhrPostResponse', (res) => {
      this.entities = this.parseEntities(res.data)
      this.$emit('getEntitiesSuccess', res.data)
      this.stopLoading()
    })
    this.$on('xhrPostError', (err) => {
      this.$emit('getEntitiesError', err)
      this.stopLoading()
    })

    if (this.auto) {
      this.getEntities()
    }
  },
  computed: {
    canGetEntities () {
      return true
    }
  },
  methods: {
    parseEntities (data) {
      return data.hits.hits.map((e) => {
        return e['_source']
      })
    },
    getEsQuery () {
      let query = this.query
      query.size = this.size
      query.from = this.from
    },
    getEntities () {
      if (this.canGetEntities) {
        this.query = this.getEsQuery()
        if (this.xhrPostUrl && this.query) {
          this.xhrPostParams = this.query
          this.performXhrPost()
        }
      }
    }
  },
  watch: {
    esUrl (n) {
      if (n) {
        this.xhrPostUrl = n
        if (this.auto) {
          this.getEntities()
        }
      }
    },
    auto (n) {
      if (n) {
        this.getEntities()
      }
    },
    esQuery (n) {
      if (n) {
        this.query = n
        if (this.auto) {
          this.getEntities()
        }
      }
    },
    size (n) {
      if (this.auto) {
        this.getEntities()
      }
    },
    from (n) {
      if (this.auto) {
        this.getEntities()
      }
    }
  }
} 
